# Microservices with Spring Boot

## Introduction

A Microservices architecture represents a way to design applications as a set of independently deployable services. These services should preferably be organized around business skills, automatic deployment, intelligent endpoints and decentralized control of technologies and data.

The purpose of this work is to show how to create several independently deployable services that communicate with each other, using the facilities offered by Spring Cloud and Spring Boot. __Spring Cloud__ provides tools for developers to quickly and easily build shared patterns of distributed systems (such as configuration, discovery, or intelligent routing services). __Spring Boot__ allows you to build Spring applications quickly as quickly as possible, minimizing the usually painful configuration time of Spring applications.

We will create the following microservices:

* __Product Service__: The main service, which offers a REST API to list a list of products.
* __Config Service__: Configuration service, whose role is to centralize the configuration files of the various microservices in a single location.
* __Proxy Service__: A gateway that handles the routing of a request to one of the instances of a service, so as to automatically manage the load distribution.
* __Discovery Service__: A service that records service instances for discovery by other services.

  ![](https://insatunisia.github.io/TP-eServices/img/tp4/archi.png)


## STEP 1 : Microservice ProductService

We start by creating the main service: Product Service.
Each microservice will be in the form of a Spring project.

To quickly and easily create a Spring project with all the necessary dependencies, we will use Spring Initializr.

* 1- Create a spring boot project using Spring Initializr https://start.spring.io/
* 2- Configure project as follow :
  * Project : Maven project
  * Language : Java
  * Group : miage.micro
  * Artifact : product-service
  * Dependencies :  
    * Web,
    * Rest Repositories,
    * JPA,
    * H2,
    * Actuator (for montoring and application management),
    * Eureka Discovery (for the integration with the Discovery Service),
    * Config Client (for the integration with the Config Service)
* 3- create the Product class

```
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Product implements Serializable {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    public Product(){
    }
    public Product(String name) {
        this.name = name;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
```
* 4- This class is annotated with JPA, to store the Product objects in the H2 database using Spring Data. To do this, create the ProductRepository interface in the same package:

```
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product , Integer> {
}
```
* 5- To insert the objects in the database, we will use the Stream object. For that, we will create the DummyDataCLR class:

```
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.stream.Stream;

@Component
class DummyDataCLR implements CommandLineRunner {

    @Override
    public void run(String... strings) throws Exception {
        Stream.of("Pencil", "Book", "Eraser").forEach(s->productRepository.save(new Product(s)));
        productRepository.findAll().forEach(s->System.out.println(s.getName()));
    }

    @Autowired
    private ProductRepository productRepository;

}
```

* 6- run and test the service http://localhost:8080
You will notice that the created REST service automatically respects the HATEOAS standard, which offers in the REST services, the links to dynamically navigate between the interfaces.
* 7- test the product service : http://localhost:8080/products and get information about the product id = 1

* 8- To add a search feature by name, for example, modify the ProductRepository interface, as follows:

```
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface ProductRepository extends JpaRepository<Product , Integer> {

    @Query("select p from Product p where p.name like :name")
    public Page<Product> productByName(@Param("name") String mc
            , Pageable pageable);
}
```
* 9- Research for product name Eraser :
http://localhost:8080/products/search/productByName?name=Eraser
* 10- Activate the Actuator metrics :
  * open src/main/resources/application.properties and add
    * management.endpoints.beans.enabled=false
    * management.endpoints.web.exposure.include=*

  * Restart the project. The result obtained by running
    * http://localhost:8080/actuator/metrics/
    * http://localhost:8080/actuator/health/

## STEP 2 : Multiple instance of Micro ProductService

To launch multiple instances of the ProductService service, we will define multiple configurations (Netbeans profile) with different port numbers. For that:

* 1- Create a new profile in Netbeans called product-service-2 ( right-click on the project, setConfiguration/customize)
* 2- Edit the "product-service-2" profile, a new argument to the RUN /Arguments field eg. --server.port=8081
* 3- etc for each new instance of ProductService



## STEP 3 : Create the Microservice ConfigService

In a microservice architecture, several services run at the same time, on different processes, each with its own configuration and its own parameters. Spring Cloud Config provides server-side and client-side support for outsourcing configurations in a distributed system. Thanks to the configuration service, it is possible to have a centralized place to manage the properties of each of these services.
  ![](https://insatunisia.github.io/TP-eServices/img/tp4/archi-s3.png)
* 1- Start by creating a ConfigService service in Spring Initializr, with the appropriate dependencies, as follow :
  * Project : Maven project
  * Language : Java
  * Group : miage.micro
  * Artifact : config-service
  * Dependencies :  
    * Config Server


* 2- Open the project on Netbeans
* 3- To expose a configuration service, use the @EnableConfigServer annotation for the ConfigServiceApplication class, as follows:

```
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class ConfigServiceApplication {

    public static void main(String[] args) {

        SpringApplication.run(ConfigServiceApplication.class, args);
    }
}
```
* 4- To configure this configuration service, add the following values ​​to its application.properties file:
  * server.port=8888
  * spring.cloud.config.server.git.uri=file:./src/main/resources/myConfig

* 5- Create the myConfig folder on src/main/resources
* 6- in myConfig folder, create a new __application.properties__ file and add
  * __global=xxxxx__

  This file will be shared between all microservices using this configuration service.

  The configuration directory must be a git directory. For that:
  Open a cmd terminal and navigate to this directory. ( From Netbeans : right-click on myConfig folder -> tools -> openTerminal )
  Initialize a git local repository on this folder:
  ```
  git init
  ```
  Create a root entry in the repository:
  ```
  git add.
  ```

  Commit:
  ```
  git commit -m "add."
  ```
* 8- run the config service
* 7- Return to the __ProductService__ project and add to the application.properties configuration file:
  * spring.application.name = product-service
  * spring.cloud.config.uri = http://localhost:8888

* 9- Restart your product services. To consult the configuration service, go to http://localhost:8888/product-service/master.


* 10-  Since the __application.properties__ file contains all the shared properties of the different microservices, we will need to add other files for each microservice specific properties. For that:

  * Add a new properties file for our ProductService service 
    -  create __product-service.properties__ file in the __myConfig__ directory  
    - add the following value :
    ```
    me=radhouene.azzabi@cea.fr
    ```
  * retest your product services http://localhost:8888/product-service/master


* 11- Return to the __ProductService__ project and create java class __ProductRestService__

```
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductRestService {

  @Value("${me}")
  private String me;

  @RequestMapping("/messages")
  public String tellMe(){
      System.out.println("hello from me, !");
      return "hello from " + me;
  }
}
```
* 12 - test the message endpoint : http://localhost:8080/messages


## STEP 4 : Create the Microservice DiscoveryService

To avoid strong coupling between microservices, it is strongly recommended to use a discovery service that allows you to record the properties of the different services and avoid having to call a service directly. Instead, the discovery service will dynamically provide the necessary information, which ensures the resilience and dynamicity of a microservice architecture.

![](https://insatunisia.github.io/TP-eServices/img/tp4/archi-s4.png)

To achieve this, Netflix offers the Eureka Service Registration and Discovery service, which we will use in our application.

* 1- create the DiscoveryService in Spring Initializr, with the appropriate dependencies, as follow :
  * Project : Maven project
  * Language : Java
  * Group : miage.micro
  * Artifact : discovery-service
  * Dependencies :  
    * Eureka Server
    * Config Client
* 2- open the projet on Netbeans
* 3- update the DiscoveryServiceApplication by adding the EnableEurekaServer annotation.

```
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class DiscoveryServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DiscoveryServiceApplication.class, args);
    }
}
```

* 4- Add the following properties to its application.properties file.
  * spring.application.name=discovery-service
  * spring.cloud.config.uri=http://localhost:8888
* 5- In the config-service project, create an discovery-service.properties file under the myConfig directory.
  * server.port = 8761
  * eureka.client.fetch-registry = false
  * eureka.client.register-with-eureka = false

* 6- run and test the Eureka service http://localhost:8761
* 7- Normaly no instance registered in the discovery server. We will therefore modify the code of the __ProductServiceApplication__ class for the __ProductService__ microservice to register:

```
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class ProductServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductServiceApplication.class, args);
    }
}
```

* 8- restart the differents ProductService (8080, 8081, ... )
and check Eureka Discovery ( normaly you we see all started ProductsServices as Registred Eureka Service)



## STEP 5 : Create the Microservice ProxyService
The microservice architecture, by providing a set of independent and loosely coupled services, faces the challenge of providing a unified interface for consumers, so they do not see the low-granular decomposition of your services. This is why the use of a proxy service, responsible for routing requests and load balancing, is important.

![](https://insatunisia.github.io/TP-eServices/img/tp4/archi-s5.png)


Netflix offers the Zuul service to do this. To create your Proxy microservice:

* 1- with Spring Initializr, create :
  * Project : Maven project
  * Language : Java
  * Group : miage.micro
  * Artifact : proxy-service
  * Dependencies :  
    * Zuul,
    * Web,
    * HATEOAS,
    * Actuator,
    * Config Client
    * Eureka Discovery

* 2- Open project with Netbeans, clean and build
* 3- Add the __@EnableZuulProxy__ annotation and the __@EnableDiscoveryClient__ to the ProxyServiceApplication class so that the proxy is also registered in this discovery service.
* 4- Add the spring.application.name and spring.cloud.config.uri properties to the proxy service application.properties file.
  * spring.application.name = proxy-service
  * spring.cloud.config.uri = http://localhost:8888

* 5- Create the proxy-service.properties file in the myConfig directory of the configuration service, where you will set the proxy service port to 9999.
  * server.port = 9999

* 6- Run the __proxy-service__ project
* 7- To test the __proxy-service__ : we will use the product-service [message](http://localhost:8080/messages) endpoint. But we have multiple instance of product-service running (8080, 8081). So the proxy will let us to request the __product-service__  meesage endpoint without having to know the product-servcie port: 
  * If you run the request http://localhost:9999/product-service/messages multiple times, you will notice that the display is me who answered and it will be displayed on the consoles of the two or three running product-service instances.
  * Finaly, n order to consume the available services, external customers must know the proxy port number and the sevice name, and the differents endpoints.  

